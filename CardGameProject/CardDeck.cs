﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    class CardDeck
    {
        private List<Card> _cardList;

        public CardDeck()
        {
            _cardList = new List<Card>();
        }
        /// <summary>
        /// Obtain the number of cards in the deck This is a read-only property
        /// </summary>
        public int CardCount
        {
            get { return _cardList.Count; }
        }
    }
}
