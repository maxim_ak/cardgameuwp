﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    struct GameScore
    {
       // Use automatic properties instead of field variables in the structure
       //private int _playerScore;
       //private int _houseScore;

       // because there is no business logic associated with the score use
       // automatic properties. AUTOMATIC properties do not use explicit field variables
       public int PlayerScore { get; set; }
       public int HouseScore { get; set; }
        public GameScore (int playerScore, int houseScore)
        {
            this.PlayerScore = playerScore;
            this.HouseScore = houseScore;
        }

    }

    class CardGame
    {
        //TODO: define read-only properties for these field variables 
        private CardDeck _deck;

        private GameScore _score;

        private Card _playerCard;

        private Card _houseCard;
        /// <summary>
        /// Initialize the game objects and its attributes
        /// </summary>
        public CardGame()
        {
            //define deck use in card game
            _deck = new CardDeck();

            //keep track of the score of the player and the house
            _score = new GameScore();

            //track the current hand, the cards currently in play in the current round
            _playerCard = null;
            _houseCard = null;

        }
        /// <summary>
        /// Plays the game rounds until no more cards are left keeping the score.
        /// </summary>
        public void Play()
        {
            sbyte roundResult = PlayRound();
        }
        /// <summary>
        /// Plays a round in the game
        /// </summary>
        /// <returns>
        /// +1: the player won the round
        /// -1: the house won the round
        ///  0: the round was a draw
        /// </returns>
        public sbyte PlayRound()
        {
            string exchangeInput;

            // determine the ranks of the player and house cards
            byte playerCardRank = DetermineCardRank(_playerCard);
            byte houseCardRank = DetermineCardRank(_houseCard);

            // determine who won the round, the player or the house
            if (playerCardRank > houseCardRank)
            {
                //player won the round
                return 1;
            }
            else if (houseCardRank > playerCardRank)
            {
                //house won the round
                return -1;
            }
            else
            {
                //the round is a draw
                return 0;
            }
        }
        /// <summary>
        /// Determines the rank of the card such that Ace has the highest rank
        /// </summary>
        /// <returns>
        /// 14: if the value is an Ace
        /// value of the card for any other card
        /// </returns>
        public byte DetermineCardRank(Card card)
        {
            // check for Ace to ensure it has the highest rank
            if (card.Value == 1)
            {
                //This is the Ace card, return highest rank
                return 14;
            }
            else
            {
                //Regular card, the rank is the same as the value
                return card.Value;
            }

        }
    }
}
